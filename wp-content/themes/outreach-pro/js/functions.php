<?php
//* Start the engine
include_once( get_template_directory() . '/lib/init.php' );

//* Set Localization (do not remove)
load_child_theme_textdomain( 'outreach', apply_filters( 'child_theme_textdomain', get_stylesheet_directory() . '/languages', 'outreach' ) );

//* Child theme (do not remove)
define( 'CHILD_THEME_NAME', __( 'Outreach Pro Theme', 'outreach' ) );
define( 'CHILD_THEME_URL', 'http://my.studiopress.com/themes/outreach/' );
define( 'CHILD_THEME_VERSION', '3.0.1' );

//* Add HTML5 markup structure
add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

//* Add viewport meta tag for mobile browsers
add_theme_support( 'genesis-responsive-viewport' );

//* Enqueue Google fonts
add_action( 'wp_enqueue_scripts', 'outreach_google_fonts' );
function outreach_google_fonts() {

	wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Lato:400,700', array(), CHILD_THEME_VERSION );
	
}

//* Enqueue Responsive Menu Script
add_action( 'wp_enqueue_scripts', 'outreach_enqueue_responsive_script' );
function outreach_enqueue_responsive_script() {

	wp_enqueue_script( 'outreach-responsive-menu', get_bloginfo( 'stylesheet_directory' ) . '/js/responsive-menu.js', array( 'jquery' ), '1.0.0' );

}

//* Add new image sizes
add_image_size( 'home-top', 1140, 460, TRUE );
add_image_size( 'home-bottom', 285, 160, TRUE );
add_image_size( 'sidebar', 300, 150, TRUE );


//* Add support for custom header
add_theme_support( 'custom-header', array(
	'header-selector' => '.site-title a',
	'header-text'     => false,
	'height'          => 100,
	'width'           => 340,
) );

//* Add support for custom background
add_theme_support( 'custom-background' );

//* Add support for additional color style options
add_theme_support( 'genesis-style-selector', array(
	'outreach-pro-blue' 	=>	__( 'Outreach Pro Blue', 'outreach' ),
	'outreach-pro-orange' 	=> 	__( 'Outreach Pro Orange', 'outreach' ),
	'outreach-pro-purple' 	=> 	__( 'Outreach Pro Purple', 'outreach' ),
	'outreach-pro-red' 		=> 	__( 'Outreach Pro Red', 'outreach' ),
) );

//* Add support for structural wraps
add_theme_support( 'genesis-structural-wraps', array(
	'header',
	'nav',
	'subnav',
	'site-inner',
	'footer-widgets',
	'footer',
) );

//* Add support for 4-column footer widgets
add_theme_support( 'genesis-footer-widgets', 4 );

//* Set Genesis Responsive Slider defaults
add_filter( 'genesis_responsive_slider_settings_defaults', 'outreach_responsive_slider_defaults' );
function outreach_responsive_slider_defaults( $defaults ) {

	$args = array(
		'location_horizontal'             => 'Left',
		'location_vertical'               => 'bottom',
		'posts_num'                       => '4',
		'slideshow_excerpt_content_limit' => '100',
		'slideshow_excerpt_content'       => 'full',
		'slideshow_excerpt_width'         => '35',
		'slideshow_height'                => '460',
		'slideshow_more_text'             => __( 'Continue Reading', 'outreach' ),
		'slideshow_title_show'            => 1,
		'slideshow_width'                 => '1140',
	);

	$args = wp_parse_args( $args, $defaults );
	
	return $args;
}

//* Hook after post widget after the entry content
add_action( 'genesis_after_entry', 'outreach_after_entry', 5 );
function outreach_after_entry() {

	if ( is_singular( 'post' ) )
		genesis_widget_area( 'after-entry', array(
			'before' => '<div class="after-entry widget-area">',
			'after'  => '</div>',
		) );

}

//* Modify the size of the Gravatar in the author box
add_filter( 'genesis_author_box_gravatar_size', 'outreach_author_box_gravatar_size' );
function outreach_author_box_gravatar_size( $size ) {

    return '80';
    
}

//* Remove comment form allowed tags
add_filter( 'comment_form_defaults', 'outreach_remove_comment_form_allowed_tags' );
function outreach_remove_comment_form_allowed_tags( $defaults ) {
	
	$defaults['comment_notes_after'] = '';
	return $defaults;

}

//* Add the sub footer section
add_action( 'genesis_before_footer', 'outreach_sub_footer', 5 );
function outreach_sub_footer() {

	if ( is_active_sidebar( 'sub-footer-left' ) || is_active_sidebar( 'sub-footer-right' ) ) {
		echo '<div class="sub-footer"><div class="wrap">';
		
		   genesis_widget_area( 'sub-footer-left', array(
		       'before' => '<div class="sub-footer-left">',
		       'after'  => '</div>',
		   ) );
	
		   genesis_widget_area( 'sub-footer-right', array(
		       'before' => '<div class="sub-footer-right">',
		       'after'  => '</div>',
		   ) );
	
		echo '</div><!-- end .wrap --></div><!-- end .sub-footer -->';	
	}
	
}

//* Register widget areas
genesis_register_sidebar( array(
	'id'          => 'home-top',
	'name'        => __( 'Home - Top', 'outreach' ),
	'description' => __( 'This is the top section of the Home page.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'home-bottom',
	'name'        => __( 'Home - Bottom', 'outreach' ),
	'description' => __( 'This is the bottom section of the Home page.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'after-entry',
	'name'        => __( 'After Entry', 'outreach' ),
	'description' => __( 'This is the after entry widget area.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'sub-footer-left',
	'name'        => __( 'Sub Footer - Left', 'outreach' ),
	'description' => __( 'This is the left section of the sub footer.', 'outreach' ),
) );
genesis_register_sidebar( array(
	'id'          => 'sub-footer-right',
	'name'        => __( 'Sub Footer - Right', 'outreach' ),
	'description' => __( 'This is the right section of the sub footer.', 'outreach' ),
) );

/* ***********************customization start here *********************** */

   function wptuts_styles_with_the_lot()
{   
    wp_register_style( 'custom-style', get_stylesheet_directory_uri() . '/bootstrap/css/bootstrap-theme.min.css','','', 'screen' );
    wp_register_style( 'custom-style1', get_stylesheet_directory_uri() . '/bootstrap/css/bootstrap-theme.css','','', 'screen');
	wp_register_style( 'custom-style2', get_stylesheet_directory_uri() . '/bootstrap/css/bootstrap.min.css','','', 'screen' );
	wp_register_style( 'custom-style3', get_stylesheet_directory_uri() . '/bootstrap/css/bootstrap.css','','', 'screen');
	wp_register_script( 'add-bx-custom-js', get_template_directory_uri() . '/bootstrap/js/bootstrap.js', '', null,''  ); 
	wp_enqueue_script( 'add-bx-custom-js' );
	wp_enqueue_style( 'custom-style' );
	wp_enqueue_style( 'custom-style1' );
	wp_enqueue_style( 'custom-style2' );
	wp_enqueue_style( 'custom-style3' );

}
add_action( 'wp_enqueue_scripts', 'wptuts_styles_with_the_lot' );


remove_action( 'genesis_header', 'genesis_do_header' );


function wptuts_scripts_basic()
{      wp_register_script( 'custom-script', get_stylesheet_directory_uri() . '/js/jquerytest.js' );
	   wp_register_script( 'custom-script1', get_stylesheet_directory_uri() . '/js/jquery.bxslider.min.js' );	
	   wp_register_style( 'custom-style99', get_stylesheet_directory_uri() . '/js/jquery.bxslider.css');
	   wp_register_script( 'custom-script2', get_stylesheet_directory_uri() . '/js/slick.js' );	
	  wp_register_style( 'custom-style100', get_stylesheet_directory_uri() . '/js/slick.css');
	
	 wp_enqueue_script( 'custom-script');
	 wp_enqueue_script( 'custom-script1');
	 wp_enqueue_style( 'custom-style99' );
	wp_enqueue_script( 'custom-script2');
	wp_enqueue_style( 'custom-style100' );
	 
		}
add_action( 'wp_enqueue_scripts', 'wptuts_scripts_basic' );






/* This is start for header area */
add_action('init','header_pos');
function header_pos()
{
add_action( 'genesis_before_header', 'wpvkp_bhw');
}
function wpvkp_bhw()
{ ?>
	<div class="container-fluide">
	  <div class="row" style="border-top: 7px solid #1e5c37;padding-top:7px;padding-bottom:7px;">
	     <div class="col-sm-1" >
	      </div> 
	  	     <div class="col-sm-4" id="logo_div" >
	  	       <div style="float:left;margin-right:10px;"><img src="<?php echo header_image(); ?>" alt="" width="121px" height="120px" />
	  	       </div>
 	           <div width="284px" height="64px" style="font-family: Arial;color:#1d5c31;font-size: 28px;font-weight: 400;letter-spacing: 0.7px;line-height: 37px;margin-left:25px;">
                <?php
                 $options = get_option('sara_general_settings_arraykey');
          		echo $options[maintagline];
          		?>
 	           </div>

 	           <div class="subtagline" width="284px" height="64px" style="font-family: Arial;color:#383637;font-size: 14px;font-weight: 400;letter-spacing: -0.7px;line-height: 40px;text-align: left;
"> <?php echo $options[subtagline];?>
             </div>
	         </div>

	         <div class="col-sm-4">
	         <?php
           if($options[radio_example]== "1")
	             {
                ?>
                     <div   style="height:88px;border-style: solid;border-width: 2px;border-color:#770606;box-sizing: border-box;
background-color:#990000;border: 7px solid #ffd454;box-shadow: 4px 8px 5px 0px rgba(0, 0, 0, 0.33);margin-left:30px;margin-right:20px;margin-top:17px;overflow:hidden;">
	             <img class="messageimg" src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/Urgent_message.png" >
	               <p  style="color:#ffffff;font-size: 19px;font-weight: 700;line-height: 31px;text-align: center;padding-top:10px;padding-left:13px;padding-right:10px;">
	                    <?php

	                                echo $options[immmessage]; 	
	                                                         
	                    ?>
	                 </p>
	             	                </div>
                  <?php
              }
              ?>
	         </div>


	         <div class="col-sm-3">
       
               <div class="socialmedia">
                 <a href=""><img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/facebook.png" style="background-color: #209c60;width:31;height:31px;border-radius: 50%;"></img></a>
                 <a href=""><img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/Tweeter.png" style="background-color: #209c60;width:31;height:31px;border-radius: 50%;"></img></a>
                 <a href=""><img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/in.png" style="background-color: #209c60;width:31;height:31px;border-radius: 50%;"></img></a>
                 <a href=""><img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/google_plus.png" style="background-color: #209c60;width:31;height:31px;border-radius: 50%;"></img></a>
                 <a href=""><img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/Play.png" style="background-color: #209c60;width:31;height:31px;border-radius: 50%;"></img></a>
                </div>
	 
	         </div>
	  	  </div>

	  </div>
	<?php  
}
/* This is end for header area */

/* **** this is start of below menubar area***** */
 /* This is start of meta slider */
 
   
 remove_action( 'genesis_after_header', 'mp_cta_genesis' );
  add_action( 'genesis_after_header', 'mp_cta_genesis' );
 
 
 function mp_cta_genesis()
   { 
    
        echo do_shortcode("[metaslider id=36]"); 
      
         ?>
       <!-- This is end of meta slider -->
    

   <!-- This is start for green div on home page -->
      <div class="container">
	    <div class="row">
	       <div class="col-sm-12" >
	             <div class='square-box' style="background-color: #209c60;">
                   <div class='square-content'>
                    <img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/oldalts.png" width="111px" height="109px"></img>
                      <p>
                        <a href="#" style="color:#ffffff;font-size: 14px;font-weight: 400;text-align: center; transform:scaleX(1.0191);"> 
                         Old Altrinchamians
                         </a>
                        </p>
                      </div>
                 </div>
                 <div class='square-box' style="background-color: #188752;">
                   <div class='square-content'>
                    <img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/gears.png" width="111px" height="109px"></img>
                      <p class="leftmargin">
                        <a href="#" style="color:#ffffff;font-size: 14px;font-weight: 400;text-align: center; transform:scaleX(1.0191);"> 
                         AGSB Engine
                         </a>
                        </p>
                      </div>
                 </div>
                 <div class='square-box' style="background-color: #107243;">
                   <div class='square-content'>
                    <img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/people.png" width="111px" height="109px"></img>
                      <p class="leftmargin">
                        <a href="#" style="color:#ffffff;font-size: 14px;font-weight: 400;text-align: center; transform:scaleX(1.0191);"> 
                       Parent Access
                         </a>
                        </p>
                      </div>
                 </div>
                 <div class='square-box' style="background-color: #075f35;">
                  <div class='square-content'>
                    <img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/message.png" width="111px" height="109px"></img>
                      <p class="leftmargin">
                        <a href="#" style="color:#ffffff;font-size: 14px;font-weight: 400;text-align: center; transform:scaleX(1.0191);"> 
                         AGSB Mail
                         </a>
                        </p>
                      </div>
                 </div>
                 <div class='square-box' style="background-color: #034a28;">
                   <div class='square-content'>
                    <img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/calender.png" width="111px" height="109px"></img>
                      <p>
                        <a href="#" style="color:#ffffff;font-size: 14px;font-weight: 400;text-align: center; transform:scaleX(1.0191);"> 
                         School Calender
                         </a>
                        </p>
                      </div>
                 </div>
                 <div class='square-box' style="background-color: #034525;">
                   <div class='square-content'>
                    <img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/contact.png" width="111px" height="109px"></img>
                      <p>
                        <a href="#" style="color:#ffffff;font-size: 14px;font-weight: 400;text-align: center; transform:scaleX(1.0191);"> 
                         Technical Support
                         </a>
                        </p>
                      </div>
                 </div>
   	       </div>
        </div>
      </div>   
<!-- This is end for green div on home page -->

    <?php 
      	 if ( is_home() || is_front_page() )
      	{
      		?>
    <!-- this is start of welcome text on home page -->
    <div class="container">
        
        <div class="row" style="height:25px;">
        </div>

	    <div class="row">
	       <div class="col-sm-12" >
	          <div style="border-bottom:thin dotted;border-top:thin dotted;">
	            <p style="font-family:OpenSans;color:#912124;font-size: 19px;font-weight: 600;text-align: left;">
	             Welcome To Altrincham Grammer School for Boys</p>
	           </div>
	        </div>
	      </div> <!-- end of second row -->   

	    <div class="row"> 
	      <div class="col-sm-12" >
            <p style="color:#666666;font-size: 14px;font-weight: 400;line-height: 22px;text-align: justify;
              padding-top:15px;">
	        Thank you for visiting the Altrincham Grammer School for Boys website. AGSB is a selective boy's grammer school 
	        providing an academic education between the ages of 11 and 18 for circa 1,250 students. Boys who attend come mainly
	        from the immediate Altrincham area (postcodes WA14 and WA15) but we also have places for pupils from further afield 
	        who use the excellent rail,train and road network to reach us from all points of the compass.
	        <br><br>
	        <a href="#" style="color:#912124;font-size: 14px;font-weight: 600;line-height: 23px;text-decoration: underline;
             text-align: left;">Read More...</a> 
            </p>
	      </div>
	    </div> <!-- end of third row -->
	 </div><!-- end of container -->      
     
        <!-- this is end of welcome text on home page -->


     <!-- this is start of news and events row on home page -->
         <br><br>
         <div class="container">
             <div class="row">
	            <div class="col-sm-8" >
	               <div  style="border-bottom:thin dotted;border-top:thin dotted;margin-bottom:5px;">
	                    <p style="color:#912124;font-size: 19px;font-weight: 600;text-align: left;">
	                      Latest News & Events 
	                     </p>
	                     
	               </div>
	                  <?php
	                        $args = array(
                            'post_type' => 'news',
                            'posts_per_page' => 5
                             );?>
                              
	                        <div class="bxslider">
  	                        <?php $obituary_query = new WP_Query($args);
                             while ($obituary_query->have_posts()) : $obituary_query->the_post();
                               $feat_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
                                 ?>
                                 <div class="row" id="newsimage">
	                               <div class="col-sm-6" >
                                      <a href="" ><img src="<?php echo "$feat_image"; ?>" style="height:110px;width:307px;border:1px solid black;margin-botom:2px;"/></a>
                                    </div>
                                    <div class="col-sm-6" >
                                       <p style="color:#1e5c37;font-size: 16px;font-weight: 400;text-align: justify;">
                                        <?php the_title(); ?>
                                       </p>
                                       <p style="color:#666666;font-size: 13px;font-weight: 400;line-height: 18px;text-align: justify">
                                        <?php
                                           $words = explode(" ",strip_tags(get_the_content()));
                                           $content = implode(" ",array_splice($words,0,22));
                                                   echo $content;
                                                    echo "<br>";
                                          ?>
                                         
                                          <a href="<?php echo get_permalink();?>" style="color:#912124;font-size: 14px;font-weight: 400;line-height: 23px;text-decoration: underline;text-align: left;">
                                             Read More</a>
                                        </p>
                                     </div>
                                 </div> <!-- end of row -->
                               
	                            <?php
                             endwhile; ?>
                               </div> <!-- end of class bxslider -->

                               <?php
                           wp_reset_postdata();                      
                      ?>
                       
               
             </div> <!-- end of column 7 -->

	          <div class="col-sm-4"  >
	            <div id="multiblog"	 style="border-style:solid;border-width: 1px;border-color:#cccccc;box-sizing: border-box;background-color:
                     rgba(255, 255, 255, 0.75);width: 340px;height: auto;float:right;">
                     <div style="border-bottom:thin dotted;">
	                      <p style="font-family:OpenSans;color:#912124;font-size: 17px;font-weight: 600;text-align:center;padding-top:10px;">
	                             Multi-Academy Trust Consultaion
	                         </p>
                     </div>
                      <div style="padding:10px;">
                         <img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/Academy_03.png" height="98px" width="304px"></img>
                          <p style="color:#666666;font-size: 13px;font-weight:400;line-height: 18px;text-align: justify;padding-top:10px;">
                          Altrincham Grammar School for Boys is currently consulting on its proposal to convert from a single academy trust
                          to a multi-academy trust(MAT). Please see below for the consultation process.letter to all groups with an interest-
                          <a href="#" style="color:#912124;font-size: 13px;font-weight: 400;line-height: 18px;
                             text-decoration: underline;text-align: justify;">Click Here
                             </a> 
                         </p>
                       </div>
                 </div>
	          </div> <!-- end of column 5 -->
	         </div><!-- end of first row -->
	     </div> <!-- end of container -->      
     <!-- this is end of news and events row on home page -->  

         <br><br>
         <div class="container">
             <div class="row" >
	            <div class="col-sm-12" style="border-bottom:thin dotted;border-top:thin dotted;" >
	                
	                      <p style="font-family:OpenSans;color:#912124;font-size: 17px;font-weight: 600;text-align:left;padding-top:10px;">
	                             Awards & Certificates
	                         </p>
                    	                    
                </div>
              </div>
          </div>       


<?php
}  /* end of if condition for home page */ 
}  /* end of function mp_cta_genesis */
/* **** this is start of below menubar area***** */





function genesischild_footerwidgetheader_position ()  
{
	  ?>
	  <div class="container">
             <div class="row">
	            <div class="col-sm-12" >
                   <?php // echo do_shortcode('[wonderplugin_carousel id="1"]');
                   
                        ?>
                        <div class="multiple-items">
                        <img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/Healthy_School_21.png" width="200" height="200">
                        <img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/Healthy_School_21.png" width="200" height="200">
                        <img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/Healthy_School_21.png" width="200" height="200">
                        <img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/Healthy_School_21.png" width="200" height="200">
                        <img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/National_Standard_15.png" width="200" height="200">
                        <img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/National_Standard_15.png" width="200" height="200">
                        <img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/National_Standard_15.png" width="200" height="200">
                        <img src="http://altrincham.wsisites.net/wp-content/uploads/2015/08/National_Standard_15.png" width="200" height="200">
                        </div>
                </div>
             </div>
       </div>      
<?php 
}

add_action ('genesis_before_footer','genesischild_footerwidgetheader_position',5);





/* this is start code for copyright footer customization */
add_filter( 'genesis_footer_creds_text', 'sp_footer_creds_text' );
function sp_footer_creds_text() {
	 ?>
	 <div class="container">
             <div class="row">
	            <div class="col-sm-3">
	             </div>
	             <div class="col-sm-7">
	               <div style="color:#209c60;font-size: 12px;font-weight: 400;line-height: 19px;text-align: center;
float:left;">COPYRIGHT@</div>
 <div style="color:#ffd900;font-size: 12px;font-weight: 400;line-height: 19px;text-align: center;float:left;">
  ALTRINCHAM GRAMMAR SCHOOL FOR BOYS 2015</div>
   <div style="color:#209c60;font-size: 12px;font-weight: 400;line-height: 19px;text-align: center;
float:left;">| ALL RIGHTS RESERVED.</div>
	             </div>
	             <div class="col-sm-2">
	             </div>
	           </div>
	      </div>       

<?php

}
/* this is end code for copyright footer customization */




/* this is start of part of option page */
 class Test_Options
   {
      function __construct()
           {
	       	add_action( 'admin_menu', array( $this, 'add_sara_settings_menu' ) );
		    add_action( 'admin_init', array( $this, 'sara_register_settings' ) );
            }
	
	function add_sara_settings_menu()
	     {
		 // add_options_page( $page_title, $menu_title, $capability, $menu_slug, $function )
		 add_menu_page( 'Sara', 'Altrincham Grammer School for Boys', 'manage_options', 'test-plugin', array($this, 'create_sara_settings_page') );
	    }
	
	function create_sara_settings_page()
	   {
	   ?>   
	       <form method="post" action="options.php">
             <?php
		            // This prints out all hidden setting fields
		            // settings_fields( $option_group )
		           settings_fields( 'sara-general-group' );
		             // do_settings_sections( $page )
		           do_settings_sections( 'sara-general-settings-section' );
                ?>
               <?php submit_button('Save Changes'); ?>
           </form>
	  <?php }
	   
	   function sara_register_settings() 
	   {
		 		// add_settings_section( $id, $title, $callback, $page )
		add_settings_section('general-settings-section','',
		          array($this, 'print_general_settings_section_info'),'sara-general-settings-section');

		// add_settings_field( $id, $title, $callback, $page, $section, $args )
		add_settings_field('some-setting','',array($this, 'create_input_General_setting'), 
			               'sara-general-settings-section', 'general-settings-section');

		// register_setting( $option_group, $option_name, $sanitize_callback )
		register_setting( 'sara-general-group', 'sara_general_settings_arraykey', 
		               array($this, 'general_additional_settings_validate') );
		
	   }

       function print_general_settings_section_info()
         {
		   echo '<p><h3>Add Tag lines</h3></p>';
		  }


	   function create_input_general_setting() {
		   $options = get_option('sara_general_settings_arraykey');
		  ?>
		 <table border="1">
       	   <tr valign="top">
			 <th scope="row"><?php _e( 'Main Tag Line' ); ?></th>
             <td>
		 	 <input class="colorpickeruse" id="sara_general_settings_arraykey[maintagline]" name="sara_general_settings_arraykey[maintagline]" type="text" value=" <?php echo $options[maintagline]; ?>" />
             </td>
			 </tr>
			 
			 	 
			  <tr valign="top">
			  <th scope="row"><?php _e( 'Sub Main Tag Line' ); ?></th>
             <td>
		 	 <input class="colorpickeruse" id="sara_general_settings_arraykey[	]" name="sara_general_settings_arraykey[subtagline]" type="text" value=" <?php echo $options[subtagline]; ?>" />
             </td>
			 </tr>

			 <tr valign="top">
			  <th scope="row"><?php _e( 'Hide or Show Immediate massage' ); ?></th>
             <td>
              <input type="radio" id="radio_example_one" name="sara_general_settings_arraykey[radio_example]" value="1"  <?php checked( 1, $options['radio_example']);  ?> />Show <br />
                <input type="radio" id="radio_example_two" name="sara_general_settings_arraykey[radio_example]" value="2"  <?php checked( 2, $options['radio_example']);  ?> />Hide <br />
		 	  </td>
			 </tr>

                 
                   <tr valign="top">
			  <th scope="row"><?php _e( 'Immediate massage' ); ?></th>
             <td>
             
		 	 <textarea  name=sara_general_settings_arraykey[immmessage] rows='4' cols='40' type='textarea'><?php echo $options['immmessage'] ?></textarea>";
		 	  </td>
			 </tr>

              			   
		 </table>
			 <?php	
     }	
	 
	 
		function general_additional_settings_validate($arr_input) 
		{
		$options['maintagline'] = trim( $arr_input['maintagline'] );
		$options['subtagline'] = trim( $arr_input['subtagline'] );			
		$options['immmessage'] = trim( $arr_input['immmessage'] );
		$options['radio_example'] = trim( $arr_input['radio_example'] );	
			return $options;
	    }
	
   }

    $ss=new Test_Options();


/* this is end of part of option page */



function create_news_post_type() {
	register_post_type( 'news',
		array(
			'labels' => array(
				'name' => 'news',
				'singular_name' => 'news',
				'add_new' => 'Add New',
				'add_new_item' => 'Add New news',
				'edit_item' => 'Edit news',
				'new_item' => 'New news',
				'view_item' => 'View news',
				'search_items' => 'Search Cafes',
				'not_found' =>  'Nothing Found',
				'not_found_in_trash' => 'Nothing found in the Trash',
				'parent_item_colon' => ''
			),
			'public' => true,
			'publicly_queryable' => true,
			'show_ui' => true,
			'query_var' => true,
			//'menu_icon' => get_stylesheet_directory_uri() . '/yourimage.png',
			'rewrite' => true,
			'capability_type' => 'post',
			'hierarchical' => false,
			'menu_position' => null,
			'supports' => array('title','editor','thumbnail')
		)
	);
}
add_action( 'init', 'create_news_post_type' );


/*
function wpb_add_google_fonts() {

wp_register_style('wpb-googleFonts', 'http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,700italic,400,700,300');
            wp_enqueue_style( 'wpb-googleFonts');
        }
    add_action('wp_print_styles', 'wpb_add_google_fonts');

*/








wp_enqueue_style( 'google-fonts', '//fonts.googleapis.com/css?family=Open+Sans', array(), CHILD_THEME_VERSION );



