<?php
/** Enable W3 Total Cache */
define('WP_CACHE', true); // Added by W3 Total Cache

/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'agsbwebsit_agsb');

/** MySQL database username */
define('DB_USER', 'agsbwebsit_agsb');

/** MySQL database password */
define('DB_PASSWORD', '5PWpFTV85pa8Hejs');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'ix!(r%t~5CHZlGL|[XyOtvwVnEtcZkQXb}r#KZ$Q*fn~O=ZA!I_qdKXEet[gEtoL');
define('SECURE_AUTH_KEY',  '9y~?1!cFT4f/S-|SxNdDNfR+{[q+aF&drfnv]Lw&vB1akTwdH<7TV)%aHT#!y%An');
define('LOGGED_IN_KEY',    'W]Ae+0tnU9a;ZjZH=FJLT`HugtO^KP$bp 6bW$JGEM[aLYfpuXZ$^I?6=Z[R!{Zb');
define('NONCE_KEY',        'lgy4|gW.0>F-&8uZGF%A ?5s%%|_J<+;PRB,%,]{<-3<^Xj&l3y-5|j`(!:^%5Fe');
define('AUTH_SALT',        '|]+k&xp|Ch-X(_bsGmPK+<a`e+T=&n6>J{sb0hs}j8+XeRA>0RN~8hI9[CC6+Bvg');
define('SECURE_AUTH_SALT', '[zkJivdH7ETRmeU#%L8yMndSuB+L(4IcmqJH)hAYH>/I2F^|*9CzgW1(Ll5kMu%<');
define('LOGGED_IN_SALT',   't$v)MPV#uyR|0Z|bD*o>Wgo[^GTVZ!3^1{#BG[k_uC:Zh%mxbp%+-@ab?d]{@uv=');
define('NONCE_SALT',       'L`{2`y(Y%|l>{(tc,:6Z,d-+qQIhYX}Pb~G>9LIL(@b.y~-!+sX!NMX5+wFavAf_');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'altrinschool_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('DISALLOW_FILE_EDIT', true);
define('AUTOMATIC_UPDATER_DISABLED', true);
